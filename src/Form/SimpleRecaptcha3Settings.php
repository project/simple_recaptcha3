<?php

namespace Drupal\simple_recaptcha3\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for a lead entity type.
 */
class SimpleRecaptcha3Settings extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_recaptcha3_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_recaptcha3.settings');

    $form['site_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Key'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#description' => $this->t('<a href="https://www.google.com/recaptcha/admin">Generate your Recaptcha Keys</a> here using recaptcha v3.'),
      '#default_value' => $config->get('site_key'),
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#default_value' => $config->get('secret_key'),
    ];

    $form['score'] = [
      '#type' => 'select',
      '#title' => $this->t('Score'),
      '#options' => [
        '0' => $this->t('0'),
        '0.1' => $this->t('0.1'),
        '0.2' => $this->t('0.2'),
        '0.3' => $this->t('0.3'),
        '0.4' => $this->t('0.4'),
        '0.5' => $this->t('0.5'),
        '0.6' => $this->t('0.6'),
        '0.7' => $this->t('0.7'),
        '0.8' => $this->t('0.8'),
        '0.9' => $this->t('0.9'),
        '1' => $this->t('1'),
      ],
      '#description' => $this->t('Threshold of spam check. 1 = extremely restrictive, 0 = extremely permissive.'),
      '#default_value' => $config->get('score') ?? '0.5',
    ];

    $form['form_blacklist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Exclude Forms'),
      '#description' => $this->t('Recaptcha is applied to all non-admin forms by default. Add form machine names here to exclude, separate by newlines.  Wildcards (*) are supported'),
      '#default_value' => $config->get('form_blacklist'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('simple_recaptcha3.settings');
    // Save new settings.
    $config->set('site_key', trim($form_state->getValue('site_key')));
    $config->set('secret_key', trim($form_state->getValue('secret_key')));
    $config->set('score', $form_state->getValue('score'));

    $original_blacklist = $config->get('form_blacklist');
    $updated_blacklist = $form_state->getValue('form_blacklist');

    // Avoid EOL issues, by splitting on \n and trimming.
    $blacklise = array_map(function ($form) {
      return trim($form);
    }, explode(PHP_EOL, $updated_blacklist));
    $updated_blacklist = implode(PHP_EOL, $blacklise);
    $blacklist_changed = ($original_blacklist !== $updated_blacklist);
    if ($blacklist_changed) {
      $config->set('form_blacklist', $updated_blacklist);
    }
    $config->save();

    $this->messenger()->addStatus($this->t('The configuration has been updated.'));

    // Clear render cache, so changes are immediate for anonymous users.
    if ($blacklist_changed) {
      drupal_flush_all_caches();
    }

  }

}
