<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Implements hook_toolbar_alter().
 */
function simple_recaptcha3_form_alter(array &$form, FormStateInterface $form_state, $form_id)
{
  // skip admin pages
  if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    return;
  }

  $config = \Drupal::config('simple_recaptcha3.settings');

  // check blacklist and bounce if blacklisted
  if (!simple_recaptcha3_should_enable($form_id, $config)) {
    return;
  }

  // this field is used to submit the recaptcha token back to server.  It can be named anything but this is standard.
  $form['g-recaptcha-response'] = [
    '#type' => 'hidden',
    '#default_value' => '',
  ];

  // attach recaptcha library.  There's a simple inline code that can be used, but Drupal makes inline JS hard,
  // so we're using parameters here instead.
  $form['#attached']['library'][] = 'simple_recaptcha3/form_captcha';
  $form['#attached']['drupalSettings']['simple_recaptcha3']['site_key'] = $config->get('site_key');
  $form['#attached']['drupalSettings']['simple_recaptcha3']['forms'][$form_id] = $form_id;

  // check the recaptcha
  $form['#validate'][] = 'simple_recaptcha3_captcha_validate';
}

function simple_recaptcha3_should_enable(string $form_id, ImmutableConfig $config): bool
{
  static $form_blacklist = NULL; // cache in case we need to call for multiple forms
  $site_key = $config->get('site_key');
  if (empty($site_key)) {
    return false;
  }
  if (is_null($form_blacklist)) {
    // check blacklist and bounce if blacklisted
    $forms = $config->get('form_blacklist');
    if (is_null($forms)) {
      $form_blacklist = [];
    } else {
      $form_blacklist = explode(PHP_EOL, $forms);
    }
  }

  foreach ($form_blacklist as $rule) {
    // pattern match each rule to see if we're on an applicable form
    if (fnmatch($rule, $form_id)) {
      return FALSE;
    }
  }
  return TRUE;
}

function simple_recaptcha3_captcha_validate(&$form, FormStateInterface $form_state)
{
  $config = \Drupal::config('simple_recaptcha3.settings');
  $secret_key = $config->get('secret_key');
  if (empty($secret_key)) {
    \Drupal::logger('simple_recaptcha3')->warning('Recaptcha Keys not set');
    $form_state->setErrorByName('g-recaptcha-response', t('Recaptcha Keys not set.'));
    return;
  }

  $verify_url = 'https://www.google.com/recaptcha/api/siteverify';

  if ($form_state->hasValue('g-recaptcha-response')) {
    $token = $form_state->getValue('g-recaptcha-response');

    //The data you want to send via POST
    $fields = [
      'secret' => $secret_key,
      'response' => $token,
      'remoteip' => \Drupal::request()->getClientIp(),
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $verify_url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
    //So that curl_exec returns the contents of the cURL; rather than echoing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $body = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if ($http_code != 200) {
      \Drupal::logger('simple_recaptcha3')->warning('captcha validation returned status code @code', ['@code' => $http_code]);
      $form_state->setErrorByName('g-recaptcha-response', t('Unable to validate captcha.'));
      return;
    }

    if (empty($body)) {
      \Drupal::logger('simple_recaptcha3')->warning('captcha validation response was empty');
      $form_state->setErrorByName('g-recaptcha-response', t('Unable to validate captcha.'));
      return;
    }

    $json = json_decode($body, TRUE);
    if (json_last_error() !== JSON_ERROR_NONE) {
      \Drupal::logger('simple_recaptcha3')->warning('captcha validation json response could not be parsed: @error', ['@error' => json_last_error_msg()]);
      $form_state->setErrorByName('g-recaptcha-response', t('Unable to validate captcha.'));
      return;
    }

    if (!empty($json['error-codes'])) {
      \Drupal::logger('simple_recaptcha3')->warning('captcha validation returned errors: @errors', ['@errors' => implode(',', $json['error-codes'])]);
      $form_state->setErrorByName('g-recaptcha-response', t('Unable to validate captcha.'));
      return;
    }

    if ($json['action'] !== $form['#form_id']) {
      \Drupal::logger('simple_recaptcha3')->warning('captcha attempt for wrong form');
      $form_state->setErrorByName('g-recaptcha-response', t('Unable to validate captcha.'));
      return;
    }

    $threshold = $config->get('score') ?? 0.5;
    if (!$json['success'] || $json['score'] < $threshold) {
      $form_state->setErrorByName('g-recaptcha-response', t('Captcha check failed.'));
      return;
    }

  }
}
