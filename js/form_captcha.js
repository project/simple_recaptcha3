/**
 * @file
 * Captcha code for front-end.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.formCaptcha = {
    attach: function (context, settings) {
      const captchaSettings = settings.simple_recaptcha3;
      const body = document.querySelector('body');

      if (body.classList.contains('simple-recaptcha3-processed') || !captchaSettings.site_key) {
        return;
      }

      const injectScript = function (url) {
        return new Promise((resolve, reject) => {
          let s = document.createElement('script');
          s.type = "text/javascript";
          s.async = true;
          s.onload = _ => {
            resolve();
          }
          s.onerror = _ => {
            reject();
          }
          s.src = url;
          document.getElementsByTagName('head')[0].appendChild(s);
        });
      }

      const formName = function (machineName) {
        return machineName.replace(/_/g, '-');
      };

      injectScript("https://www.google.com/recaptcha/api.js?render=" + captchaSettings.site_key)
        .then(_ => {

          body.addEventListener('submit', (event) => {
              let currentFormId = null;
              const form = event.target;

              for (const formId in captchaSettings.forms) {
                if (form.id === formName(formId)) {
                  currentFormId = formId;
                  break;
                }
              }
              if (currentFormId == null) {
                return;
              }

              event.preventDefault();

              grecaptcha.ready(_ => {
                grecaptcha.execute(captchaSettings.site_key, {action: currentFormId}).then((token) => {
                  form.querySelector("input[name=g-recaptcha-response]").value = token;
                  form.submit();
                });
              });
            }
          );
        });

      // This is intentionally outside the promise, so we don't stampede
      // while it's loading the script.
      body.classList.add('simple-recaptcha3-processed');
    }
  };
})(jQuery, Drupal);
