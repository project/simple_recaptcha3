# Simple Recaptcha3

## SUMMARY

The simple recaptcha3 module is a simple way to enable recaptcha3 on your site.

Due to problems with the Captcha module documented [here](https://www.drupal.org/project/recaptcha/issues/3035883),
it's impossible to use the captcha-module-based recaptcha or recaptcha_3 modules on production
sites with caching enabled.
Captcha module is designed for captchas that require data to be persisted between form display
and validation, which results in some performance loss as pages with forms cannot
be cached.  Some effort was made to add support for cacheable captchas, but this support still
relies on some session data being stored which is unnecessary and causes recaptcha modules to fail.

With most sites needing only one captcha, we've dropped the captcha backend as it's not designed
specifically for uncacheable captchas and unneccesary for recaptcha 2/3.

Additionally, we've found that in most of the Drupal sites we support, we generally want captcha
enabled on all forms with few exceptions, so this module works with a blacklist instead of whitelist
approach.  All forms have recaptcha3 enabled unless added to the blacklist.


## REQUIREMENTS

None.


## INSTALLATION

* Install as usual, see http://drupal.org/node/1897420 for further information.

* Visit the recaptcha3 administration page at /admin/config/simple_recaptcha3 to set
site and secret keys, spam threshold and blacklist any forms, if desired.


## CONFIGURATION

* Configure user permissions in Administration » People » Permissions:

  - Administer recaptcha3

    This allows users to set the site & secret keys and blacklist forms.

* Customize the menu settings in Administration » User Interface » Recaptcha3


## TROUBLESHOOTING

* If recaptcha is improperly configured under Google's admin console, you may
be unable to log in.  If this happens, make sure that your domain is allowed
in Google's console.  If you are unable, but need to log in to update your
site & secret keys, this module can be disabled via command line via
`drush pmu -y simple_recaptcha` .
